const LINE_COLOR = "#000000";
const LINE_COLOR_FIRST = "#00FF00";
const LINE_COLOR_LAST = "#FF0000";
const LINE_WIDTH = 2;
const LINE_DASH = [0.1, 0.07];

const WIDTH = 400;
const HEIGHT = 400;
const PAD = 20;

// Ova je metoda namijenjena inicijalizaciji zadatka.
// Dobiva questionConfigData koji postavlja editor konfiguracije
// -------------------------------------------------------------------------------------------------
function questionInitialize(questionConfig) {
  const angleIndex = Math.floor(questionConfig.allowed_angles.length * Math.random());
  const angle = questionConfig.allowed_angles[angleIndex];

  const lineCountRange = questionConfig.line_count[1] - questionConfig.line_count[0] + 1;
  const lineCount = Math.floor(Math.random() * lineCountRange) + questionConfig.line_count[0];

  let correct = "";
  let points = [];

  let currentAngle = 0;
  let currentX = 0;
  let currentY = 0;
  points.push({ x: currentX, y: currentY });

  //TODO: somehow prevent lines from overlapping
  // This doesn't happen often, but there isn't an easy way to fix.
  // The easy way would be to check if there are already the same points in the `points` array
  // and then change the direction (from - to + or vice versa), but if it overlaps there as well
  // its problematic. A fix would be to then go back one line and take a different direction.
  // This requires a stack and the line generation effectively becomes DFS.
  for (let i = 0; i < lineCount; i++) {
    let sign = Math.random() < 0.5 ? "+" : "-";

    correct += "F" + (i < lineCount - 1 ? sign : "");

    currentX += Math.cos(currentAngle / 180 * Math.PI);
    currentY -= Math.sin(currentAngle / 180 * Math.PI);
    let newPoint = { x: currentX, y: currentY };
    points.push(newPoint);

    if (sign === "+") {
      currentAngle += parseInt(angle);
    } else {
      currentAngle -= parseInt(angle);
    }
  }

  userData.question = { angle: angle, points: points };
  userData.question.correct = correct;
  userData.questionState = "";
}

// Ova je metoda namijenjena generiranju varijabli koje se dinamički računaju i nigdje ne pohranjuju.
// Ove varijable bit će vidljive kodu koji HTML-predloške odnosno ServerSide-JavaScript-predloške
// lokalizira.
// -------------------------------------------------------------------------------------------------
function getComputedProperties() {
  return { computedProperty: userData.question.answer };
}

// Ova metoda poziva se kako bi se obavilo vrednovanje zadatka. Pozivatelju treba vratiti objekt:
// {correctness: X, solved: Y}, gdje su X in [0,1], Y in {true,false}.
// -------------------------------------------------------------------------------------------------
function questionEvaluate() {
  function setCorrectSolution() {
    userData.correctQuestionState = userData.question.correct;
  }

  var res = { correctness: 0.0, solved: false };
  if (!userData.questionState) {
    setCorrectSolution();
    return res;
  }
  res.solved = true;


  userData.questionState = userData.questionState.replace(/ /g, '');
  if (userData.questionState === userData.question.correct) {
    res.correctness = 1.0;
    userData.correctQuestionState = "" + userData.questionState;
  } else {
    res.correctness = 0.0;
    setCorrectSolution();
  }

  return res;
}

// Metoda koja se poziva kako bi izvadila zajednički dio stanja zadatka koje je potrebno za prikaz zadatka.
// -------------------------------------------------------------------------------------------------
function exportCommonState() {
  return { angle: userData.question.angle, points: userData.question.points };
}

// Metoda koja se poziva kako bi izvadilo stanje zadatka koje je postavio korisnik (njegovo rješenje).
// Razlikuje se svrha prikaza: korisnikovo rješenje ili točno rješenje
// -------------------------------------------------------------------------------------------------
function exportUserState(stateType) {
  if (stateType == "USER") {
    return { qs: userData.questionState };
  }
  if (stateType == "CORRECT") {
    return { qs: userData.correctQuestionState };
  }
  return {};
}

// Metoda koja se poziva kako bi vratila stanje zadatka koje odgovara nerješavanom zadatku.
// -------------------------------------------------------------------------------------------------
function exportEmptyState() {
  return { qs: "" };
}

// Metoda koja se poziva kako bi pohranila stanje zadatka na temelju onoga što je korisnik napravio.
// -------------------------------------------------------------------------------------------------
function importQuestionState(data) {
  userData.questionState = data;
}

/*@#common-include#(constants.js)*/
var api = {
  initQuestion: function (cst, st) {
    document.getElementById(questionLocalID + "_answer").readOnly = readOnly;
    document.getElementById(questionLocalID + "_answer").value = st.qs;
    document.getElementById(questionLocalID + "_angle_1").innerHTML = cst.angle;
    document.getElementById(questionLocalID + "_angle_2").innerHTML = cst.angle;

    let canvas = document.getElementById(questionLocalID + "_canvas");
    let ctx = canvas.getContext("2d");
    canvas.width = WIDTH;
    canvas.height = HEIGHT;
    ctx.lineCap = 'round';

    const points = cst.points;
    let br = boundingRect(points);
    const REAL_WIDTH = WIDTH - 2 * PAD, REAL_HEIGHT = HEIGHT - 2 * PAD;
    const BR_WIDTH = br.maxX - br.minX, BR_HEIGHT = br.maxY - br.minY;
    const scale = Math.min(REAL_WIDTH / BR_WIDTH, REAL_HEIGHT / BR_HEIGHT);

    // Scale line width properly
    ctx.lineWidth = LINE_WIDTH / scale;
    // Move to be inside of padded area
    ctx.translate(PAD, PAD);
    // Scale canvas to size
    ctx.scale(scale, scale);
    // Translate into view
    ctx.translate(-br.minX, -br.minY);
    // Center within the rectangle
    ctx.translate((REAL_WIDTH / scale - BR_WIDTH) / 2, (REAL_HEIGHT / scale - BR_HEIGHT) / 2);

    for (let i = 0; i < points.length - 1; i++) {
      let p1 = points[i];
      let p2 = points[i + 1];

      ctx.strokeStyle = LINE_COLOR;

      ctx.beginPath();
      ctx.moveTo(p1.x, p1.y);

      if (i == 0) {
        ctx.strokeStyle = LINE_COLOR_FIRST;
      } else if (i == points.length - 2) {
        ctx.strokeStyle = LINE_COLOR_LAST;
        ctx.setLineDash(LINE_DASH);
      }

      ctx.lineTo(p2.x, p2.y);
      ctx.stroke();
    }
  },
  takeState: function () {
    return document.getElementById(questionLocalID + "_answer").value;
  },
  resetState: function (cst, st) {
    document.getElementById(questionLocalID + "_answer").value = '';
  },
  revertState: function (cst, st) {
    document.getElementById(questionLocalID + "_answer").value = st.qs;
  }
};

/**
 * Finds the 2D bounding rectangle which contains all the points given.
 * @param {Array} points array of points in the form of {x: value, y: value}.
 */
function boundingRect(points) {
  let br = { minX: undefined, minY: undefined, maxX: undefined, maxY: undefined };

  for (let p of points) {
    if (p.x < br.minX || br.minX == undefined) {
      br.minX = p.x;
    }

    if (p.y < br.minY || br.minY == undefined) {
      br.minY = p.y;
    }

    if (p.x > br.maxX || br.maxX == undefined) {
      br.maxX = p.x;
    }

    if (p.y > br.maxY || br.maxY == undefined) {
      br.maxY = p.y;
    }
  }

  return br;
}

return api;
